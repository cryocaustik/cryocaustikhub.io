$(document).ready(function(){
  loadGitHub();
});

function loadGitHub() {
  var repos;

  $.ajax({
      url: 'https://api.github.com/users/cryocaustik/repos',
      type: 'GET',
      dataType: 'JSON'
    })
    .done(function(data) {
      var repos = data;

      var repoCont = $('#repo-container');
      var repoHtml = '';
      $.each(repos, function(r_indx, repo) {
        repoHtml += '<a href="' + repo.svn_url + '" class="nav-item nav-link">' + repo.name + '</a>';
      })
      repoCont.html(repoHtml);
    })
    .fail(function(request) {
      console.log(request);
    });

  $.ajax({
      url: 'https://api.github.com/users/cryocaustik',
      type: 'GET',
      dataType: 'JSON'
    })
    .done(function(r_data) {
      var curHtml = $('.navbar-brand').html(),
        imgHtml = '<img class="img-fluid bg-dark rounded float-left" src="' + r_data.avatar_url + '" alt=">.>" height="30" width="30">';
      $('.navbar-brand').html(imgHtml + ' ' + curHtml);
    })
    .fail(function(request) {
      console.log('profile fail: ', request);
    })
}